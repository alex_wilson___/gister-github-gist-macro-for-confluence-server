package me.davidsimpson.confluence.addon.gister;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;

import java.util.Map;

/**
 * User: david
 * Date: 20/06/2013
 * Time: 20:08
 */
public class GistMacro implements Macro
{
    public GistMacro() { }
    
    @Override
    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }
    
    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
    
    @Override
    public String execute(Map<String, String> parameters,
                          String body,
                          ConversionContext conversionContext)
            throws MacroExecutionException
    {
        String url = parameters.get("url");
        
        if (url.startsWith("https://gist.github"))
        {
            return "<script src=\"" + url + ".js\"></script>";
        } 
        else
        {
            return "<div class=\"aui-message\"><p class=\"title\"><span class=\"aui-icon icon-generic\"></span><strong>Sorry, I didn't catch the gist of that. Did you get the URL right?</strong></p></div>";
        }
    }
}