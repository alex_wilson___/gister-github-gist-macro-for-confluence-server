// https://developer.atlassian.com/display/CONFDEV/Plugin+Tutorial+-+Extending+Autoconvert


/**
 * Autoconvert links into a macro for gist
 *
 * URL:
 *   https://gist.github.com/dvdsmpsn/6733853
 *
 * converts to:
 *   <ac:macro ac:name="gist">
 *       <ac:parameter ac:name="url">https://gist.github.com/dvdsmpsn/6733853.js</ac:parameter>
 *   </ac:macro>
 */
(function(){
    AJS.bind("init.rte", function($) {

        var dvdsmpsn = {};
        dvdsmpsn.Confluence = {};
        dvdsmpsn.Confluence.Gister = {

            pasteHandler: function(uri, node, done) {

                var directoryParts = uri.directory.split("/");

                if (uri.host == 'gist.github.com') {
                    if (uri.directory.split("/").length == 3) {
                        var macro = {'name':'xhtml-gist-macro', 'params': {'url': 'https://gist.github.com' + uri.directory }};
                        tinymce.plugins.Autoconvert.convertMacroToDom(macro, done, done);
                    } else {
                        done();
                    }
                } else {
                    done();
                }
            }
        };
        tinymce.plugins.Autoconvert.autoConvert.addHandler(dvdsmpsn.Confluence.Gister.pasteHandler);
    });
})();


