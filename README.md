# Gister - Effortlessly embed gists into Confluence

## What

A Confluence add-on that does just one thing effortlessly.

Got a gist on Github? Want to embed it in Confluence?

Paste the gist URL into the Confluence editor and your gist will automatically be embedded using the *Gist* macro.

# → [Download Gister for Confluence 1.0](https://dvdsmpsn.wufoo.com/forms/gister-for-confluence-get-it-for-free/)

[See a video demo of the *Gist* macro in Gister](http://www.youtube.com/watch?v=5PhAaN6HJdc)

![Gister](https://bytebucket.org/dvdsmpsn/gister-github-gist-macro-for-confluence/raw/a9bde2f7f05d10db86f8c31b0c43e46297b2882a/src/main/resources/images/pluginLogo.png "Gister - Gist macro in Confluence")


## Definition

> _"One who gets the gist of a subject matter and knows enough to be dangerous with that subject in conversation or writing."_
>
> - Definition of "gister" on [Urban Dictionary](http://www.urbandictionary.com/define.php?term=gister)

## Support

Gister supports Confluence 5.2 and above.

For support and feature requests:

- [Create an Issue](https://bitbucket.org/dvdsmpsn/gister-github-gist-macro-for-confluence-cloud/issues)
- [Fill out the contact form](http://davidsimpson.me/contact-me/)
- [Email](mailto:david@davidsimpson.me?subject=Gister%20for%20Confluence%3A%20Support)


## License

Gister is licensed using the [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0).